<?php

use App\Schema\NormString;
use App\Schema\InputMask;
use App\Schema\Thumbnail;
use App\Schema\FileUpload;
use Norm\Schema\Reference;
use App\Schema\SelectTwoReference;
use App\Schema\MultiReference;
use App\Schema\DatePicker;
use App\Schema\SysparamReference;
use App\Schema\SearchReference;
use App\Schema\Editor;
use Norm\Schema\NormDate;
use Norm\Schema\NormDateTime;
 

return array(
	// 'observers' => array(
    //     'App\\Observer\\PasienObserver' => null,
    // ),
    'schema' => array(
        'nama_pasien' => SelectTwoReference::create('nama_pasien')->to('Pasien','$id','nama')->set('list-column',true),
        'code' => NormString::create('code')->set('list-column',true),
        'tanggal' => NormDate::create('tanggal','Tanggal')->set('list-column', true),
        'status_kunjungan' => SysparamReference::create('status_kunjungan')->setGroups('status_kunjungan')->set('list-column', true),
        'stage' => SysparamReference::create('stage')->setGroups('stage')->set('list-column', true),
        'deskripsi' => Editor::create('deskripsi'),
    ),
);
