<?php

use App\Schema\NormString;
use App\Schema\SelectTwoReference;

return array(
    // 'observers' => array(
    //     'App\\Observer\\CodeGenerateObserver' =>null,
    // ),


    'schema' => array(

        'kabupaten_id' => SelectTwoReference::create('kabupaten_id')->to('Kabupaten', '$id', 'nama')->set('list-column', true),
        'nama' => NormString::create('nama')->set('list-column', true),
    ),


);
