<?php

use App\Schema\NormString;
use App\Schema\InputMask;
use App\Schema\Thumbnail;
use App\Schema\FileUpload;
use Norm\Schema\Reference;
use App\Schema\SelectTwoReference;
use App\Schema\MultiReference;
use App\Schema\DatePicker;
use App\Schema\SysparamReference;
use App\Schema\SearchReference;
use App\Schema\Editor;
use Norm\Schema\NormDate;
use Norm\Schema\NormDateTime;
 

return array(
    // 'observers' => array(
    //     'App\\Observer\\PasienObserver' => null,
    // ),
    'schema' => array(
        'pasien_code' => SelectTwoReference::create('pasien_code')->set('hidden',true)->to('Pasien','$id','code'),
        'nama_wali' => NormString::create('nama_wali')->set('list-column', true),
        'tgl_lahir_wali' => NormDate::create('tgl_lahir_wali','Tanggal Lahir')->set('list-column', true),
        'hubungan' => NormString::create('hubungan')->set('list-column', true),
        'pekerjaan_wali'=> SysparamReference::create('pekerjaan_wali')->setGroups('pekerjaan'),
        'provinsi_id' => SelectTwoReference::create('provinsi_id')->to('Provinsi','$id','nama'),
        'kabupaten_id' => SelectTwoReference::create('kabupaten_id')->to('Kabupaten','$id','nama'),
        'kecamatan_id' => SelectTwoReference::create('kecamatan_id')->to('Kecamatan','$id','nama'),
        'alamat' => NormString::create('alamat'),  
        'telp' => NormString::create('telp'),  
    ),
);