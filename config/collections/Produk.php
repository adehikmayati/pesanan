<?php

use App\Schema\NormString;
use \App\Schema\SelectTwoReference;


 

return array(
    'schema' => array(
        'brand_id' => SelectTwoReference::create('brand_id')->to('Brand', '$id', 'nama')->set('list-column', true),
        'nama'   => NormString::create('nama')->set('list-column', true),
           ),
);