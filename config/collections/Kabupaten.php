<?php

use App\Schema\NormString;
use App\Schema\SelectTwoReference;


return array(
    'observers'=>array   (
        'App\\Observer\\State'=>null
    ),
    
   

    'schema' => array(

        'provinsi_id'=> SelectTwoReference::create('provinsi_id')->to('Provinsi','$id','nama')->set('list-column', true),
               'nama' => NormString::create('nama')->set('list-column', true),
    ),


);
