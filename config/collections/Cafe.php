<?php

use App\Schema\NormString;
use \App\Schema\SelectTwoReference;
use App\Schema\DatePicker;




 

return array(
    'observers' => array(
        'App\\Observer\\CodePesanObserver' => null
    ),

    'schema' => array(
        'tanggal' => DatePicker::create('tanggal')->setformatdate('dd-mm-yy')->set('list-column', true),
        'code_pesan' => NormString::create('code_pesan')->set('list-column', true)->set('hidden',true),
        
        'menu_id' => SelectTwoReference::create('menu_id')->to('Menu', '$id', 'nama')->set('list-column', true),

           ),
);