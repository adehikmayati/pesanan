<?php

use App\Schema\NormString;
use App\Schema\InputMask;
use App\Schema\Thumbnail;
use App\Schema\FileUpload;
use Norm\Schema\Reference;
use App\Schema\SelectTwoReference;
use App\Schema\MultiReference;
use App\Schema\DatePicker;
use App\Schema\SysparamReference;
use App\Schema\SearchReference;
use App\Schema\Editor;
use Norm\Schema\NormDate;
use Norm\Schema\NormDateTime;


return array(
    'observers' => array(
        'App\\Observer\\PasienObserver' => null,
    ),
    'schema' => array(
        'code' => NormString::create('code')->set('hidden', true)->set('list-column', true),
        'nama' => NormString::create('nama')->filter('trim|required')->set('list-column', true),
        'jenis_kelamin' => SysparamReference::create('jenis_kelamin', 'Jenis Kelamin')->setGroups('gender'),
        'tgl_lahir' => DatePicker::create('tgl_lahir', 'Tanggal Lahir')->setformatdate('dd-mm-yyyy')->set('list-column', true),
        'agama' => SysparamReference::create('agama')->setGroups('agama'),
        'suku' => SysparamReference::create('suku')->setGroups('suku'),
        'status_kawin' => SysparamReference::create('status_kawin')->setGroups('status kawin'),
        'pekerjaan' => SysparamReference::create('pekerjaan')->setGroups('pekerjaan'),
        'provinsi_id' => SelectTwoReference::create('provinsi_id')->to('Provinsi', '$id', 'nama'),
        'kabupaten_id' => SelectTwoReference::create('kabupaten_id')->to('Kabupaten', '$id', 'nama'),
        'kecamatan_id' => SelectTwoReference::create('kecamatan_id')->to('Kecamatan', '$id', 'nama'),
        'alamat' => NormString::create('alamat')->set('list-column', true),
    ),
);
