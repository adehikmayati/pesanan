<?php

use App\Schema\NormString;
use \App\Schema\SelectTwoReference;
// use Norm\Schema\NormDate;
use App\Schema\DatePicker;



 

return array(
    'observers'=>array   (
        'App\\Observer\\CodePesanObserver'=>null
    ),
    'schema' => array(
        'tanggal' => DatePicker::create('tanggal')->setformatdate('dd-mm-yy')->set('list-column', true),
        'code_pesan' => NormString::create('code_pesan')->set('hidden',true),
        
        'brand_id' => SelectTwoReference::create('brand_id')->to('Brand', '$id', 'nama')->set('list-column', true),
        'produk_id' => SelectTwoReference::create('produk_id')->to('Produk', '$id', 'nama')->set('list-column', true),

           ),
);