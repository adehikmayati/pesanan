<?php

namespace App\Controller;

use \Norm\Controller\NormController;

class PasienController extends AppController
{
    public function mapRoute()
    {
        parent::mapRoute();
        $this->map('/null/searchs', 'searchs')->via('GET', 'POST');
        $this->map('/:id/update', 'update')->via('GET', 'POST');
    }

    public function update($id)
    {

        $entry = $this->collection->findOne($id);
        $dataWali = \Norm::factory('WaliPasien')->findOne(array(
            'pasien_code' => $entry['code']
        ));
        $pekerjaanWali = \Norm::factory('Sysparam')->find(array(
            'groups' => 'pekerjaan'
        ));

        parent::update($id);

        $this->data['entry'] = $entry;
        $this->data['dataWali'] = $dataWali;
        $this->data['pekerjaan_wali'] = $pekerjaanWali;

        return $entry;
    }

    public function read($id)
    {
        parent::read($id);
        $entry = $this->collection->findOne($id);
        $this->data['wali'] = $this->readwali($entry);
        $this->data['datapekerjaan'] = $this->readwali($entry);
    }
    public function readwali($entry)
    {

        $data = \Norm::factory('WaliPasien')->findOne(
            array(
                'pasien_code' => $entry['code']
            )
        );

        $datapekerjaan = \Norm::factory('Sysparam')->findOne(array(
            'groups' => 'pekerjaan'
        ));

        return $data;
        return $datapekerjaan;
    }

    public function searchs()
    {
        $search = $this->request->get();
        $searchdata = array();
        if (count($search)) {
            foreach ($search as $key => $value) {
                if (!empty($value)) {
                    $searchdata[$key] = $value;
                }
            }
        }
        $pasien = \Norm::factory('Pasien')->find($searchdata);
        $this->data['pasien'] = $pasien;

        $searchdata = array();
        if (count($search)) {
            foreach ($search as $key => $value) {
                if (!empty($value)) {
                    $searchdata[$key] = $value;
                }
            }
        }
        $pasien = \Norm::factory('Histori')->find($searchdata);
        $this->data['history'] = $pasien;

        return $search;

        // $entry = \Norm::factory('Histori')->find($searchdata);
        // $arr = array();
        // $pasien = \Norm::factory('Pasien')->find($searchdata);
        // echo '<pre>';
        // foreach($pasien as $key1 => $value){
        //     $history = \Norm::factory('Histori')->find(array(
        //         'nama_pasien'=>$value['$id'],
        //     ));
        //     foreach($history as $key2 => $value2){
        //         $arr[$value['nama']][$value2['$id']] = $value2;
        //     }
        // }
        // $this->data['pasien'] = $arr;
        // return $pasien;

    }
}
