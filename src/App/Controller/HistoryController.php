<?php

namespace App\Controller;

use \Norm\Controller\NormController;

class HistoryController extends AppController
{
    public function mapRoute()
    {
        parent::mapRoute();
        // $this->map('/null/searchs', 'searchs')->via('GET', 'POST');
        $this->map('/null/jointest', 'jointest')->via('GET', 'POST');
    }

    // public function searchs()
    // {
    //     $search = $this->request->get();
    //     $searchdata = array();
    //     // $searchdata = array(
    //     //     "alamat!like" => $search['cari']
    //     // );

    //     //percobaan pertama
    //     // $pasien = \Norm::factory('Pasien')->find($searchdata);
    //     // // $datas = array();
    //     // // foreach($searchdata as $key1 => $value1){
    //     // //     foreach($value1 as $key2 => $value2){
    //     // //         $datas[$key1][$key2] = $value2;
    //     // //     }
    //     // // }
    //     // $this->data['pasien'] = $pasien;

    //     //percobaan 2
    //     // $datas = array();
    //     // foreach($searchdata as $key1 => $value1){
    //     //     foreach($value1 as $key2 => $value2){
    //     //         // $datas[$key1][$key2] = $value2;
    //     //         $pasien = \Norm::factory('Pasien')->findOne(array(
    //     //             'nama!like' => $value2,
    //     //             'alamat!like' => $value2,
    //     //             'code!like' => $value2,
    //     //         ));
    //     //     }
    //     // }
    //     // $this->data['pasien'] = $pasien;

    //     //percobaan 3
    //     $search = $this->request->get();
    //     $searchdata = array();
    //     if (isset($search['cari'])) {
    //         $pasien = \Norm::factory('Pasien')->find(array(
    //             "nama!like" => $search['cari'],
    //             "alamat!like" => $search['alamat'],
    //             // "code!like"=>$_GET['code']
    //         ));
    //     } else {
    //         $pasien = \Norm::factory('Pasien')->find();
    //     }
    //     $this->data['pasien'] = $pasien;


    //     // $search = $this->request->get();
    //     // $searchdata = array();
    //     // if(count($search)){
    //     //     foreach($search as $key => $value){
    //     //         if(!empty($value)){
    //     //             $searchdata[$key] = $value;
    //     //         }
    //     //     }
    //     // }
    //     // // $entry = \Norm::factory('Histori')->find($searchdata);
    //     // $arr = array();
    //     // $pasien = \Norm::factory('Pasien')->find($searchdata);
    //     // foreach($pasien as $key1 => $value){
    //     //     $history = \Norm::factory('Histori')->find(array(
    //     //         'nama_pasien'=>$value['$id'],
    //     //     ));
    //     //     foreach($history as $key2 => $value2){
    //     //         $arr[$value['nama']][$value2['$id']] = $value2;
    //     //     }
    //     // }
    //     // $this->data['pasien'] = $arr;
    //     // return $pasien;

    // }

    public function search()
    {
        $limit = $this->app->config('bono.providers')['Norm\Provider\NormProvider']['collections']['default']['limit'];
        $search = $this->request->get();

        //fix dengan menggunakan query join
        // $data = array();
        // if (empty($search['search'])) {
        //     $search['search'] = 0;
        // }

        // $sql = "
        //     SELECT pasien.nama as nama, 
        //     pasien.alamat as alamat, 
        //     pasien.tgl_lahir as tanggal_lahir,
        //     histori.stage as stage, 
        //     histori.deskripsi as description,
        //     histori.tanggal as tanggal
        //     FROM pasien JOIN histori on pasien.id = histori.nama_pasien WHERE 
        //     pasien.id =  " . $search['search'];

        // $query = parent::rowsArray($sql);

        // if (!empty($query)) {
        //     foreach ($query as $keys => $values) {
        //         $data[$keys] = $values;
        //     }
        // }

        // $this->data['pasien'] = $data;
        // $this->data['limit'] = $limit;

        //dengan norm
        // $search = $this->request->get();
        // $searchdata = array();
        // if (count($search)) {
        //     foreach ($search as $key => $value) {
        //         if (!empty($value)) {
        //             $searchdata[$key] = $value;
        //         }
        //     }
        // }
        // $pasien = \Norm::factory('Pasien')->find($searchdata);
        // $this->data['pasien'] = $pasien;

        // $search = $this->request->get();
        // $searchdata = array();
        // if (count($search)) {
        //     foreach ($search as $key => $value) {
        //         if (!empty($value)) {
        //             $searchdata[$key] = $value;
        //         }
        //     }
        // }

        // $arr = array();
        // $pasien = \Norm::factory('Pasien')->find($searchdata);
        // foreach($pasien as $key1 => $value){
        //     $history = \Norm::factory('Histori')->find(array(
        //         'nama_pasien'=>$value['$id'],
        //     ));
        //     foreach($history as $key2 => $value2){
        //         $arr[$value['nama']][$value2['$id']] = $value2;
        //     }
        // }
        // $this->data['history'] = $arr;
        // return $pasien;

        //dengan norm 
        $datahistory = array();
       //s echo '<pre>';
        if (empty($search['search'])) {
            $pasien = \Norm::factory('Pasien')->find();
            $history = \Norm::factory('Histori');
        } else {
            $pasien = \Norm::factory('Pasien')->find($search['search']);
            $history = \Norm::factory('Histori')->find(array(
                'nama_pasien' => $search['search']
            ));
            // $sysparam = \Norm::factory('Sysparam')->find(array(
            //     'groups' => 'status_kunjungan'
            // ));
        }

        $this->data['pasien'] = $pasien;
        $this->data['history'] = $history;
        $this->data['limit'] = $limit;
    }

    // public function jointest()
    // {

    //     $search = $this->request->get();
    //     $data = array();
    //     if (empty($search)) {
    //         $search['cari'] = '';
    //     }
    //     $sql = "
    //         SELECT pasien.nama as nama, 
    //         pasien.alamat as alamat, 
    //         histori.stage as stage, 
    //         histori.deskripsi as description 
    //         FROM pasien JOIN histori on pasien.id = histori.nama_pasien WHERE 
    //         nama LIKE '%" . $search['cari'] . "%' OR pasien.alamat LIKE '%" . $search['cari'] . "%'
    //         ";
    //     $query = parent::rowsArray($sql);

    //     if (!empty($query)) {
    //         foreach ($query as $keys => $values) {
    //             $data[$keys] = $values;
    //         }
    //     }

    //     $this->data['pasien'] = $data;
    // }
}
