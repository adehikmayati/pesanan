<?php

namespace App\Controller;

use \Norm\Controller\NormController;

class CafeController extends AppController
{
    public function mapRoute()
    {
        parent::mapRoute();
        // $this->map('/null/list', 'lists')->via('GET');
        $this->map('/null/export_csv', 'export_csv')->via('GET', 'POST');
        $this->map('/:id/trash', 'trash')->via('GET', 'POST');
    }
    // public function lists()
    // {
    //     var_dump("hallo");
    // }



    public function create()
    {

        $entry = $this->collection->newInstance()->set($this->getCriteria());


        $this->data['entry'] = $entry;

        if ($this->request->isPost()) {
            try {


                $body = $this->request->getBody();
                $body['status'] = '1';
                // echo "<pre>";
                //     print_r($body);

                // exit;

                foreach ($body['datas'] as $key => $datas) {
                    // for ($i = 0; $i < $datas; $i++) {
                    $entry = \Norm::factory('Cafe')->newInstance();
                    $entry->set('tanggal', $body['tanggal']);
                    $entry->set($datas);
                    $entry->set('status', $body['status']);
                    $entry->save();


                    // }
                }


                h('notification.info', $this->clazz . ' created.');

                h('controller.create.success', array(
                    'model' => $entry
                ));
            } catch (Stop $e) {
                throw $e;
            } catch (Exception $e) {
                h('controller.create.error', array(
                    'model' => $entry,
                    'error' => $e,
                ));

                // rethrow error to make sure notificationmiddleware know what todo
                throw $e;
            }
        }
    }
    public function search()
    {
        if (isset($_GET['find_data'])) {
            $find = '';
            $query = array();
            //tanggal only
            if (isset($_GET['tanggal']) && !isset($_GET['bulan']) && !isset($_GET['tahun'])) {
                $find = $find . date('Y-m-') . $_GET['tanggal'];
            }

            //bulan only
            if (!isset($_GET['tanggal']) && isset($_GET['bulan']) && !isset($_GET['tahun'])) {
                $find = $find . date('Y-') . $_GET['bulan'];
            }

            //tahun only
            if (!isset($_GET['tanggal']) && !isset($_GET['bulan']) && isset($_GET['tahun'])) {
                $find = $find . $_GET['tahun'];
            }

            //tanggal bulan only
            if (isset($_GET['tanggal']) && isset($_GET['bulan']) && !isset($_GET['tahun'])) {
                $find = $find . date('Y-') . $_GET['bulan'] . '-' . $_GET['tanggal'];
            }

            //bulan tahun only
            if (!isset($_GET['tanggal']) && isset($_GET['bulan']) && isset($_GET['tahun'])) {
                $find = $find . $_GET['tahun'] . '-' . $_GET['bulan'];
            }

            //tanggal tahun only
            if (isset($_GET['tanggal']) && !isset($_GET['bulan']) && isset($_GET['tahun'])) {
                $find = $find . $_GET['tahun'] . date('-m-') . $_GET['tanggal'];
            }

            //full date
            if (isset($_GET['tanggal']) && isset($_GET['bulan']) && isset($_GET['tahun'])) {
                $find = $find . $_GET['tahun'] . '-' . $_GET['bulan'] . '-' . $_GET['tanggal'];
            }

            $entries = $this->collection->find(array('tanggal!like' => $find, 'status' => 1))
                ->match($this->getMatch())
                ->sort($this->getSort())
                ->skip($this->getSkip())
                ->limit($this->getLimit());

            if ($_GET['tanggal!gte'] != '' && $_GET['tanggal!lte'] == '') {
                //start date only
                $entries = $this->collection->find(array('tanggal!gte' => $_GET['tanggal!gte'], 'status' => 1))
                ->match($this->getMatch())
                ->sort($this->getSort())
                ->skip($this->getSkip())
                ->limit($this->getLimit());
            } elseif ($_GET['tanggal!lte'] !='' && $_GET['tanggal!gte']=='') {
                //end date only
                $entries = $this->collection->find(array('tanggal!lte' => $_GET['tanggal!lte'], 'status' => 1))
                ->match($this->getMatch())
                ->sort($this->getSort())
                ->skip($this->getSkip())
                ->limit($this->getLimit());
            } elseif ($_GET['tanggal!lte'] !='' && $_GET['tanggal!gte']!='') {
                //both
                $entries = $this->collection->find(array('tanggal!lte' => $_GET['tanggal!lte'],'tanggal!gte' => $_GET['tanggal!gte'],'status' => 1))
                ->match($this->getMatch())
                ->sort($this->getSort())
                ->skip($this->getSkip())
                ->limit($this->getLimit());
            }

            // echo "<pre>"; print_r($find); exit;
            
        } else {
            $entries = $this->collection->find($this->getCriteria())
                ->match($this->getMatch())
                ->sort($this->getSort())
                ->skip($this->getSkip())
                ->limit($this->getLimit());
        }
        $this->data['entries'] = $entries;
    }
   
}