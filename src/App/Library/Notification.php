<?php

namespace App\Library;

use Bono\App;

class Notification
{
    public static function emailSend($data)
    {
        $app = App::getInstance();
        $config = $app->config('mail');
        $configemail = $config;

        if ($app->getMode() == 'production') {
            $transport = (new \Swift_SmtpTransport($configemail['host'], $configemail['port']))
            ->setUsername($configemail['user'])
            ->setPassword($configemail['password']);
        } else {
            $transport = (new \Swift_SmtpTransport($configemail['host'], $configemail['port']))
            ->setUsername($configemail['user'])
            ->setPassword($configemail['password']);
        }

        $mailer = new \Swift_Mailer($transport);
        foreach ($data as $key => $value) {

            try {
                $message = (new \Swift_Message($value['subject']))
                    ->setFrom([$configemail['mailfrom'] => $configemail['from']])
                    ->setTo([$value['email']])
                    ->setBody($value['content'])
                    ->setContentType("text/html");
                $send = $mailer->send($message);
                
                if ($send) {
                    return true;
                } else {
                    return false;
                }
            } catch (\Exception $e) {
                echo "<pre>";
                print_r($e);
                exit;
                return false;
            }
        }
    }
}
