<?php

namespace App\Provider;

use \Norm\Filter\Filter;
use Norm\Filter\FilterException;
use App\Library\Notification;

class ForgotPasswordProvider extends AppProvider
{
    public function initialize()
    {
        $app = $this->app;

        $app->filter('auth.authorize', function ($options) use ($app) {
            if (is_bool($options)) {
                return $options;
            }

            $segments = $app->request->getSegments();
            $segment = isset($segments[1]) ? $segments[1] : null;

            if (
                $segment == 'forgot_password' ||
                $segment == 'testing_email' ||
                $segment == 'reset_password'
            ) {
                return true;
            }

            return $options;
        });

        $app->get('/forgot_password', function() use ($app){
            $app->response->template('user.forgot');
        });

        $app->post('/forgot_password', function() use ($app){
            $app->response->template('user.forgot');
            $post = $app->request->post();

            $entry = \Norm::factory('User')->findOne(array('email' => $post['email']));

            try {
                if (empty($post['email'])) {
                    throw new \Exception("Kolom email harus diisi");
                }
                if (!$entry) {
                    throw new \Exception("Maaf email yang anda input belum terdaftar");
                }

                $entry->set('status', 3);
                $entry->save(['filter' => false]);

                $forgot = $this->saveUserForgot($entry);
                $link = \URL::site('reset_password') .'/'. $forgot['token'];
                $content = $this->app->theme->partial(
                    'email.forgot_password',
                    array(
                        'link' => $link
                    )
                );
                $data = array(
                    array(
                        'email' => $post['email'],
                        'subject' => 'My App',
                        'content' => $content
                    ),
                );

                Notification::emailSend($data);

                h('notification.info', 'Silahkan cek email untuk melanjutkan');    
                $this->app->redirect('login');
            } catch (\Slim\Exception\Stop $e) {
                throw $e;
            } catch (\Exception $e) {
                $app->response->setStatus(401);
                h('notification.error', $e);
            }

            $app->response->data('entry', $entry);

        });

        $app->get('/reset_password/:token', function($token) use ($app){
            $cek = \Norm::factory('UserForgotPassword')->findOne(array('token' => $token, 'status' => 1));

            if (!$cek) {
                ## Page Not Found
                $this->notFound('404', 'Halaman tidak ditemukan, mungkin anda sudah mengganti password');
            } else {
                if (date('Y-m-d H:i:s') > $cek['expired_date']) {
                    ## Token Expired
                    $this->notFound('404', 'Maaf, Halaman tidak ditemukan karena token sudah kadaluarsa silahkan kembali ke halaman forgot untuk mendapatkan token yang baru');
                } else {
                    $app->response->template('user.reset');
                }
            }

        });

        $app->post('/reset_password/:token', function($token) use ($app){
            $app->response->template('user.reset');
            $post = $app->request->post();

            $cek = \Norm::factory('UserForgotPassword')->findOne(array('token' => $token));

            try {
                $user = \Norm::factory('User')->findOne($cek['user_id']);
                $user->set($post);
                $user->set('status', 1);
                $user->save();
                
                $cek->set('status', 2);
                $cek->save();

                h('notification.info', 'Password Berhasil diubah, Silahkan login untuk melanjutkan');    
                $this->app->redirect('login');
            } catch (\Slim\Exception\Stop $e) {
                throw $e;
            } catch (\Exception $e) {
                $app->response->setStatus(401);
                h('notification.error', $e);
            }

            // $app->response->data('entry', $cek);
        });
    }

    public function saveUserForgot($data) {
        $collection = \Norm::factory('UserForgotPassword')->newInstance();
        $collection->set('user_id', $data->getId());
        $collection->set('token', md5($this->generateToken(20)));
        $collection->set('expired_date', date('Y-m-d H:i:s', strtotime('1 hour')));
        $collection->set('status', 1);
        $collection->save();

        return $collection;
    }
}
