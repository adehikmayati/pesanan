<?php

namespace App\Provider;

use \Norm\Filter\Filter;
use Norm\Filter\FilterException;
use App\Library\Notification;

class AppProvider extends \Bono\Provider\Provider
{
    public function initialize()
    {
        $app = $this->app;

        $app->get('/froala', function () use ($app){
            $app->response->template('notfound.test');
        });
    }

    public function generateToken($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function notFound ($status = '404', $reason = 'The page you are looking for does not exist.')
    {
        $data = array(
            'status' => $status,
            'reason' => $reason
        );
        $this->app->response->data('data', $data);
    	$this->app->response->template('notfound.404');
    }
}
