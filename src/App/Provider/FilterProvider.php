<?php

namespace App\Provider;

use \Norm\Filter\Filter;
use Norm\Filter\FilterException;
use App\Library\Notification;

class FilterProvider extends \Bono\Provider\Provider
{
    public function initialize()
    {
        $app = $this->app;

        Filter::register('uniq', function($key, $value, $model) {
        	
        	$cek = \Norm::factory($model->getClass())->findOne(array($key => $value));
        	
        	if ($cek) {
                throw new FilterException("Sorry data '".$key."' is already exist");
            }

            return $value;
        });
    }
}
