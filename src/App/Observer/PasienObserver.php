<?php

namespace App\Observer;

class PasienObserver

{
    protected $datawali;
    public function saving($model)
    {
        // echo"<pre>";
        // print_r($model->toArray());
        // exit;
        if (empty($model['code'])) {
            $model['code'] = $this->generateCode();
        }else{
            $model['code'];
        }
        $this->datawali = $model['wali'];
        // echo"<pre>";
        // print_r( $model['wali']);
        // exit;
        unset($model['wali']);

        return $model;
    }
    public function saved($model)
    {
        $this->SaveDataWali($model, $this->datawali);
    }

    public function generateCode()
    {
        $code = "HRP";
        $generatecode = \Norm::factory('GenerateCode')->findOne(array(
            'code' => $code,

        ));
        if (empty($generatecode)) {
            $generatecode = \Norm::factory('GenerateCode')->newInstance();
            $generatecode->set('code', $code);
            $generatecode->set('sequence', 0);
            $generatecode->save();
        }
        $angka = $generatecode['sequence'] + 1;
        $urutan = sprintf("%05s", $angka);
        $generatecode->set('sequence', $urutan);
        $generatecode->save();

        $result = $code . '-' . $urutan;
        return $result;
    }

    public function SaveDataWali($data, $datawali)
    {
        // echo '<pre>';
        // print_r($datawali);


        $entry = \Norm::factory('WaliPasien')->findOne(array(
            'pasien_code' => $data['code']
        ));
        if (!$entry) {
            $entry = \Norm::factory('WaliPasien')->newInstance();
        }
        $entry->set('pasien_code', $data['code']);
        $entry->set($datawali);
        $entry->save();
    }
}
