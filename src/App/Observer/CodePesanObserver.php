<?php

namespace App\Observer;

class CodePesanObserver
{

    public function saving($model)
    {
        $model['code_pesan'] = $this->CodeBarang($model->getClass());

          return $model;
    }

    public function CodeBarang($param)
    {
        $huruf = "C";
        $cekSequence = \Norm::factory('CodePesanan')->findOne(array(
            'nama_tabel' => $huruf,
        ));

        if (empty($cekSequence)) {
            $cekSequence = \Norm::factory('CodePesanan')->newInstance();
            $cekSequence->set('nama_tabel', $huruf);
            $cekSequence->set('urutan', 0);
            $cekSequence->save();
        }

        $angka = $cekSequence['urutan'] + 1;
        $urutan=sprintf("%03s",$angka);
        $cekSequence->set('urutan', $angka);
        $cekSequence->save();

        $gabung = $huruf.$urutan;
        // echo '<pre>';
        // print_r($gabung);
        // exit;
        return $gabung;
    }
   
}
