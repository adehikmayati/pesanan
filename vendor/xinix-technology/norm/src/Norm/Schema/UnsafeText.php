<?php

namespace Norm\Schema;

class UnsafeText extends NormText
{
    public function prepare($value)
    {
        return utf8_encode($value);
    }
}
