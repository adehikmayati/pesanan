@extends('layout')
<?php

use ROH\Util\Inflector;
?>
@section('pagetitle')
{{ f('pagetitle', l('Update {0}', Inflector::humanize(f('controller')->getClass()))) }}
@stop

@section('sub-header')
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">{{ f('pagetitle', l('Update {0}', Inflector::humanize(f('controller')->getClass()))) }}</h5>
            <!--end::Page Title-->

            <!--begin::Breadcrumb-->
            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                <li class="breadcrumb-item">
                    <a href="{{ f('controller.url') }}" class="text-muted">{{ f('pagetitle', l('{0}', Inflector::humanize(f('controller')->getClass()))) }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ f('controller.url', '/:id') }}" class="text-muted">{{ f('pagetitle', l('Detail {0}', Inflector::humanize(f('controller')->getClass()))) }}</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ f('controller.url', '/:id/update') }}" class="text-muted">{{ f('pagetitle', l('Update {0}', Inflector::humanize(f('controller')->getClass()))) }}</a>
                </li>
            </ul>
            <!--end::Breadcrumb-->

        </div>
        <!--end::Info-->
        <!--begin::Toolbar-->
        <div class="d-flex align-items-center">
            <!--begin::Button-->
            <a href="#" class="btn btn-light font-weight-bold mr-2" onclick="window.history.back()">
                <span class="svg-icon">
                    <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-02-01-052524/theme/html/demo1/dist/../src/media/svg/icons/Navigation/Arrow-left.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24" />
                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-12.000000, -12.000000) " x="11" y="5" width="2" height="14" rx="1" />
                            <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) " />
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                Back
            </a>
            <button type="button" class="btn btn-light-primary font-weight-bold" onclick="$('#createform').submit ()">
                <span class="svg-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24" />
                            <path d="M17,4 L6,4 C4.79111111,4 4,4.7 4,6 L4,18 C4,19.3 4.79111111,20 6,20 L18,20 C19.2,20 20,19.3 20,18 L20,7.20710678 C20,7.07449854 19.9473216,6.94732158 19.8535534,6.85355339 L17,4 Z M17,11 L7,11 L7,4 L17,4 L17,11 Z" fill="#000000" fill-rule="nonzero" />
                            <rect fill="#000000" opacity="0.3" x="12" y="4" width="3" height="5" rx="0.5" />
                        </g>
                    </svg>
                </span>
                Submit
            </button>
        </div>
        <!--end::Toolbar-->
    </div>
</div>
<!--end::Subheader-->
@stop

@section('fields')
<div class="card card-custom">
    <div class="card-body">
        <form method="post" class="kt-form kt-form--label-right" id="createform">
            <h5>DATA PASIEN</h5>
            <hr>
            @foreach ($entry->schema() as $name => $field)
            <div class="form-group row">
                @if (!$field['hidden'])
                <div class="col-xl-3 col-lg-3 col-form-label">
                    {{ $field->label() }}
                </div>
                <div class="col-lg-9 col-xl-9">
                    {{ $entry->format($name, 'input') }}
                </div>


                @endif
            </div>
            @endforeach
            <h5>DATA WALI PASIEN</h5>

            <hr>
           

            <div class="form-group row">
                <div class="col-xl-3 col-lg-3 col-form-label">
                    <label for="nama_wali">Nama Wali</label>
                </div>
                <div class="col-lg-9 col-xl-9">
                    <input type="text" name="wali[nama_wali]" class="form-control" 
                    value="{{$dataWali['nama_wali']}}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xl-3 col-lg-3 col-form-label">
                    <label for="tgl_lahir_wali">Tanggal Lahir Wali</label>

                </div>
                <div class="col-lg-9 col-xl-9">
                    <input  name="wali[tgl_lahir_wali]"  class="form-control" 
                    {{ $dataWali->format('tgl_lahir_wali', 'input') }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xl-3 col-lg-3 col-form-label">
                    <label for="hubungan">Hubungan</label>

                </div>
                <div class="col-lg-9 col-xl-9">
                    <input type="text" name="wali[hubungan]"  class="form-control" 
                    value="{{$dataWali['hubungan']}}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xl-3 col-lg-3 col-form-label">
                    <label for="pekerjaan" class="control-lable">Pekerjaan Wali</label>
                </div>
                <div class="col-lg-9 col-xl-9">
                    <select name="wali[pekerjaan_wali]"   {{ $dataWali->format('pekerjaan_wali', 'input') }}
                      </select>

                </div>
            </div>
            <div class="form-group row">
                <div class="col-xl-3 col-lg-3 col-form-label">
                    <label for="" class="control-lable">Provinsi</label>
                </div>
                <div class="col-lg-9 col-xl-9">
                    <select name="wali[provinsi_id]"  class="form-control" 
                    {{ $dataWali->format('provinsi_id', 'input') }} </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xl-3 col-lg-3 col-form-label">
                    <label for="" class="control-lable">Kabupaten</label>
                </div>
                <div class="col-lg-9 col-xl-9">
                    <select name="wali[kabupaten_id]"  class="form-control" 
                    {{ $dataWali->format('kabupaten_id', 'input') }} </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xl-3 col-lg-3 col-form-label">
                    <label for="" class="control-lable">Kecamatan</label>
                </div>
                <div class="col-lg-9 col-xl-9">
                    <select name="wali[kecamatan_id]"  class="form-control" 
                    {{ $dataWali->format('kecamatan_id', 'input') }} </select>
                </div>t 
            </div>
            <div class="form-group row">
                <div class="col-xl-3 col-lg-3 col-form-label">
                    <label for="alamat">Alamat</label>

                </div>
                <div class="col-lg-9 col-xl-9">
                    <input type="text" name="wali[alamat]"  class="form-control" 
                    value="{{$dataWali['alamat']}}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xl-3 col-lg-3 col-form-label">
                    <label for="telp">Telp</label>
                </div>

                <div class="col-lg-9 col-xl-9">
                    <input type="text" name="wali[telp]" class="form-control" 
                    value="{{$dataWali['telp']}}">
                </div>
            </div>
        </form>
    </div>
</div>
@stop
@section('customjs')
<script>

    $(function(){
        alert('hoi')
          
    })
    // $('select[name=kabupaten_id]').find('option');
    // $('select[name=kecamatan_id]').find('option');
    $('select[name=provinsi_id]').on('change', function(e) {
        $('select[name=kabupaten_id]').find('option').remove();
        $('select[name=kecamatan_id]').find('option').remove();
        var idProvinsi = $(this).val();
        //   console.log(idProvinsi);e.preventDefault();
        var url = base_url + 'index.php/kabupaten.json?provinsi_id=' + idProvinsi;
        //  console.log(url);
        //  e.preventDefault();
        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                if (data.entries.length > 0) {
                    //    var html='';
                    var html = '<option disabled selected>-- Pilih Kabupaten --</option>';
                    $.each(data.entries, function(key, value) {
                        var id = value.$id;
                        var nama = value.nama;
                        html += '<option value="' + id + '">' + nama + '</option>';
                    });
                    $('select[name=kabupaten_id]').append(html);
                }
            }
        })
    })

    $('select[name=kabupaten_id]').on('change', function(e) {
        $('select[name=kecamatan_id]').find('option').remove();
        var idKabupaten = $(this).val();
        var url = base_url + 'index.php/kecamatan.json?kabupaten_id=' + idKabupaten;

        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                if (data.entries.length > 0) {
                    // var isiDistrics='';
                    var isiKecamatan = '<option disabled selected>-- Pilih Kecamatan --</option>';
                    $.each(data.entries, function(key, value) {
                        var id = value.$id;
                        var nama = value.nama;
                        isiKecamatan += '<option value="' + id + '">' + nama + '</option>';
                    });
                    $('select[name=kecamatan_id]').append(isiKecamatan);
                }
            }
        })
    })
  

    
    // $('select[name="wali[provinsi_id]"]').val();
    // $('select[name="wali[kabupaten_id]"]').find('option').remove();
    // $('select[name="wali[kecamatan_id]"]').find('option').remove();
    $('select[name="wali[provinsi_id]"]').on('change', function(e) {
        $('select[name="wali[kabupaten_id]"]').find('option').remove();
        $('select[name="wali[kecamatan_id]"]').find('option').remove();
        var idProvinsi = $(this).val();
        //   console.log(idProvinsi);
        var url = base_url + 'index.php/kabupaten.json?provinsi_id=' + idProvinsi;
        //  console.log(url);
        //  e.preventDefault();
        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                if (data.entries.length > 0) {
                    //    var html='';
                    var html = '<option disabled selected>-- Pilih Kabupaten --</option>';
                    $.each(data.entries, function(key, value) {
                        var id = value.$id;
                        var nama = value.nama;
                        html += '<option value="' + id + '">' + nama + '</option>';
                    });
                    $('select[name="wali[kabupaten_id]"]').append(html);
                }
            }
        })
    })

    $('select[name="wali[kabupaten_id]"]').on('change', function(e) {
        $('select[name="wali[kecamatan_id]"]').find('option').remove();
        var idKabupaten = $(this).val();
        var url = base_url + 'index.php/kecamatan.json?kabupaten_id=' + idKabupaten;

        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {
                if (data.entries.length > 0) {
                    // var isiDistrics='';
                    var isiKecamatan = '<option disabled selected>-- Pilih Kecamatan --</option>';
                    $.each(data.entries, function(key, value) {
                        var id = value.$id;
                        var nama = value.nama;
                        isiKecamatan += '<option value="' + id + '">' + nama + '</option>';
                    });
                    $('select[name="wali[kecamatan_id]"]').append(isiKecamatan);
                }
            }
        })
    })



    // document.getElementById("select2-provinsi-pn-container").style.color = "red";
</script>
@stop
