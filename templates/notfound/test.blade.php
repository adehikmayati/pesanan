<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="{{ Theme::base('assets/js/froala-editor/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="example"></div>
</body>
</html>

<script src="https://code.jquery.com/jquery-3.6.0.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="{{ Theme::base('assets/js/froala-editor/js/froala_editor.pkgd.min.js') }}"></script>
<script>
	var editor = new FroalaEditor('#example');
</script>