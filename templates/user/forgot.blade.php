@extends('login')
@section('pagetitle')
    {{ f('page.title', 'Unauthorized') }}
@stop
@section('content')
<div class="login-form login-signin">
	<!--begin::Form-->
	<form class="form" novalidate="novalidate" id="kt_login_signin_form" method="post">
		<!--begin::Title-->
		<div class="pb-13 pt-lg-0 pt-5">
			<h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Forgot Password</h3>
		</div>
		<!--begin::Title-->
		<!--begin::Form group-->
		<div class="form-group">
			<label class="font-size-h6 font-weight-bolder text-dark">Email</label> 
			<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="email" name="email" autocomplete="off" />
			<span>* pastikan alamat email benar</span>
		</div>
		<!--end::Form group-->
		<!--begin::Action-->
		<div class="pb-lg-0 pb-5">
			<a href="{{ URL::site('login') }}" class="btn btn-light font-weight-bold font-size-h6 px-8 py-4 my-3 mr-3">
                <span class="svg-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24"/>
                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-12.000000, -12.000000) " x="11" y="5" width="2" height="14" rx="1"/>
                            <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                Back
            </a>
			<button type="submit" id="kt_login_signin_submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</button>
		</div>
		<!--end::Action-->
	</form>
	<!--end::Form-->
</div>
@stop