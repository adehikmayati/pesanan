
<?php  
	use \App\Schema\Password;
?>

@extends('login')
@section('pagetitle')
    {{ f('page.title', 'Unauthorized') }}
@stop
@section('content')
<div class="login-form login-signin">
	<!--begin::Form-->
	<form class="form" novalidate="novalidate" id="kt_login_signin_form" method="post">
		<!--begin::Title-->
		<div class="pb-13 pt-lg-0 pt-5">
			<h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Reset Password</h3>
		</div>
		<!--begin::Title-->
		<!--begin::Form group-->
		<div class="form-group">
			<label class="font-size-h6 font-weight-bolder text-dark">New Password</label>
			{{ Password::create('password')->formatInput(@$_GET['password']) }}
		</div>
		<!--end::Form group-->
		<!--begin::Action-->
		<div class="pb-lg-0 pb-5">
			<button type="submit" id="kt_login_signin_submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Kirim</button>
			<span>Back to login ? <a href="{{ URL::site('login') }}" title="">here</a></span>
		</div>
		<!--end::Action-->
	</form>
	<!--end::Form-->
</div>
@stop