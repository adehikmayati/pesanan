@extends('layout')
<?php
use ROH\Util\Inflector;
use App\Library\Pagination;
?>
<?php
$schema = array();
foreach (f('controller')->schema() as $key => $field) {
    if ($field['list-column']) {
        $schema[$key] = $field;
    }
}
?>
@section('pagetitle')
   {{ Inflector::pluralize(Inflector::humanize(f('controller')->getClass())) }}
@stop

@section('sub-header')
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">{{ l('{0}', Inflector::humanize(f('controller')->getClass())) }}</h5>
            <!--end::Page Title-->
            <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

            <!--begin::Search Form-->
            <div class="d-flex align-items-center" id="kt_subheader_search">
                <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">{{count($pasien)}} Total</span>
                @section('form-search')
                <form method="get" class="form-inline">
                    <div class="form-group mx-sm-3 mb-2" >
                        <select name="search"></select>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2"> search </button>
                </form>
                
                @show
            </div>
            <!--end::Search Form-->
        </div>
        <!--end::Info-->
        <!--begin::Toolbar-->
        <div class="d-flex align-items-center">
            <!--begin::Button-->
            <a href="#" class=""></a>
            <!--end::Button-->
            <!--begin::Button-->
            <a href="{{ f('controller.url', '/null/create') }}" class="btn btn-light-primary font-weight-bold ml-2">
                <span class="svg-icon">
                    <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-02-01-052524/theme/html/demo1/dist/../src/media/svg/icons/Navigation/Plus.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect fill="#000000" x="4" y="11" width="16" height="2" rx="1"/>
                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000) " x="4" y="11" width="16" height="2" rx="1"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                Add {{ l('{0}', Inflector::humanize(f('controller')->getClass())) }}
            </a>
            <a href="{{f('controller.url','/:mutliid/trash')}}" data-title="Delete Selected Record" class="btn btn-light-danger font-weight-bold ml-2 btn-multi-delete">
                <span class="svg-icon">
                    <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-02-01-052524/theme/html/demo1/dist/../src/media/svg/icons/Navigation/Check.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24"/>
                            <path d="M6.26193932,17.6476484 C5.90425297,18.0684559 5.27315905,18.1196257 4.85235158,17.7619393 C4.43154411,17.404253 4.38037434,16.773159 4.73806068,16.3523516 L13.2380607,6.35235158 C13.6013618,5.92493855 14.2451015,5.87991302 14.6643638,6.25259068 L19.1643638,10.2525907 C19.5771466,10.6195087 19.6143273,11.2515811 19.2474093,11.6643638 C18.8804913,12.0771466 18.2484189,12.1143273 17.8356362,11.7474093 L14.0997854,8.42665306 L6.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.999995, 12.000002) rotate(-180.000000) translate(-11.999995, -12.000002) "/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                Delete Selected
            </a>
        </div>
        <!--end::Toolbar-->
    </div>
</div>
<!--end::Subheader-->
@stop

@section('fields')
<h5>DATA PASIEN</h5>
<hr>
<table class="table mb-10">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Code</th>
      <th scope="col">Nama Pasien</th>
      <th scope="col">Alamat</th>
    </tr>
  </thead>
  <tbody>
    <?php
        $i = 1;
        foreach($pasien as $key => $value){
            echo"<tr>";
            echo"<td>".$i++."</td>";
            echo"<td>".$value['code']."</td>";
            echo"<td>".$value['nama']."</td>";
            echo"<td>".$value['alamat']."</td>";
            echo"</tr>";
        }
    ?>
  </tbody>
</table>
<div class="mt-3">
    <h5>DATA HISTORY PASIEN</h5>
    <hr>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                <th scope="col">No</th>
                <th scope="col">Tanggal Masuk</th>
                <th scope="col">Stage</th>
                <th scope="col">Deskripsi</th>
                </tr>
            </thead>
        <tbody>
        <?php
            $i = 1;
            foreach($history as $key => $value){
                echo"<tr>";
                echo"<td>".$i++."</td>";
                echo"<td>".$value['tanggal']->format('d/m/Y')."</td>";
                echo"<td>".$value['stage']."</td>";
                echo"<td>".$value['deskripsi']."</td>";
                echo"</tr>";
            }
        ?> 
        </tbody>
    </table>
</div>
@stop
@section('customjs')
<script>
    $('select[name="search"]').select2({
        width: "100%",
        placeholder: "search",
        ajax:{
            url: "{{URL::site('/pasien.json')}}",
            data: function(params) {
                var limit = "{{ $limit }}"
                    var query = {
                        "!search": params.term,
                        "!skip": params.page * limit - limit || 0,
                    }
                    return query
            },
            
            processResults: function(res) {
                var page = false
                    if(res.entries.length > 0){
                        page = true
                    }
                console.log(res)
                    return {
                        results: $.map(res.entries, function(item) {
                            return {
                                id: item.$id,
                                text: item.nama + ' - ' + item.alamat
                            }
                        }),
                        pagination: {
                            more: page,
                        }
                    }
                }
        }
    });

</script>
@stop
@section('customcss')
@stop
