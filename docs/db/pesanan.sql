-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 09 Jun 2021 pada 09.07
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bono-v7`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan`
--

CREATE TABLE `pesanan` (
  `id` int(11) NOT NULL,
  `code_pesan` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pesanan`
--

INSERT INTO `pesanan` (`id`, `code_pesan`, `tanggal`, `brand_id`, `produk_id`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(1, 'C001', '2021-06-09', 2, 4, 1, '1', '1', '2021-06-09 13:42:29', '2021-06-09 13:42:29'),
(2, 'C002', '2021-06-09', 2, 5, 1, '1', '1', '2021-06-09 13:42:30', '2021-06-09 13:42:30'),
(3, 'C006', '2021-06-11', 4, 5, 0, '1', '1', '2021-06-09 13:44:19', '2021-06-09 13:44:46'),
(4, 'C005', '2021-06-11', 4, 4, 0, '1', '1', '2021-06-09 13:44:19', '2021-06-09 13:44:41');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
